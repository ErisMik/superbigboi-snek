use serde::{Deserialize, Serialize};

use crate::game::models::Movement;

#[derive(Serialize, Deserialize, PartialEq, Eq, Debug)]
pub struct Index {
    #[serde(rename = "apiversion")]
    api_version: String,
    author: String,
    color: String,
    head: HeadType,
    tail: TailType,
}

impl Index {
    pub fn new(
        api_version: String,
        author: String,
        color: String,
        head: HeadType,
        tail: TailType,
    ) -> Index {
        Index {
            api_version,
            author,
            color,
            head,
            tail,
        }
    }
}

#[derive(Serialize, Deserialize, PartialEq, Eq, Debug)]
#[serde(rename_all = "lowercase")]
pub enum HeadType {
    Regular,
    Beluga,
    Bendr,
    Dead,
    Evil,
    Fang,
    Pixel,
    Safe,
    #[serde(rename = "sand-worm")]
    SandWorm,
    Shades,
    Smile,
    Tongue,
}

#[derive(Serialize, Deserialize, PartialEq, Eq, Debug)]
#[serde(rename_all = "lowercase")]
pub enum TailType {
    Regular,
    #[serde(rename = "block-bum")]
    BlockBum,
    Bolt,
    Curled,
    #[serde(rename = "fat-rattle")]
    FatRattle,
    Freckled,
    Hook,
    Pixel,
    #[serde(rename = "round-bum")]
    RoundBum,
    Sharp,
    Skinny,
    #[serde(rename = "small-rattle")]
    SmallRattle,
}

#[derive(Serialize, Deserialize, PartialEq, Eq, Debug)]
pub struct Move {
    #[serde(rename = "move")]
    movement: Movement,
}

impl Move {
    pub fn new(movement: Movement) -> Move {
        Move { movement }
    }
}
