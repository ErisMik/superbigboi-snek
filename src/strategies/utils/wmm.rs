use crate::game::models::Movement;
use std::collections::HashMap;

#[derive(Debug)]
pub struct WeightedMovementMatrix {
    matrix: HashMap<Movement, i32>,
}

const DEFAULT_VALUE: i32 = 0;

impl WeightedMovementMatrix {
    pub fn new() -> Self {
        let mut matrix = HashMap::new();
        matrix.insert(Movement::Left, DEFAULT_VALUE);
        matrix.insert(Movement::Right, DEFAULT_VALUE);
        matrix.insert(Movement::Up, DEFAULT_VALUE);
        matrix.insert(Movement::Down, DEFAULT_VALUE);

        WeightedMovementMatrix { matrix }
    }

    pub fn shift_score(&mut self, movement: &Movement, value: i32) {
        let new_value = self.matrix[movement] + value;
        self.matrix.insert(movement.clone(), new_value);
    }

    pub fn only_two_positive_options(&self) -> bool {
        let mut positive_actions = 0;

        for (_, value) in self.matrix.iter() {
            if value >= &0 {
                positive_actions += 1;
            }
        }

        return positive_actions == 2;
    }

    pub fn select_best_movement(&self) -> Option<Movement> {
        let mut best_movement = None;
        let mut best_score = std::i32::MIN;

        for (movement, score) in self.matrix.iter() {
            if score > &best_score {
                best_score = *score;
                best_movement = Some(movement.clone());
            }
        }

        return best_movement;
    }
}
