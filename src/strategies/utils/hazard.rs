use crate::game::models::{Point, Turn};
use std::collections::HashMap;

#[derive(Hash, Clone, PartialEq, Eq, Debug)]
pub enum Hazard {
    Body,
    Edge,
    Food,
    Gap,
    Head,
}

pub type HazardMap = HashMap<Point, Hazard>;

pub fn generate_hazard_map(turn: &Turn) -> HazardMap {
    let mut hazard_map = HashMap::new();

    // Outside edges of the map
    for x in 0..turn.board.width {
        for _ in 0..turn.board.height {
            hazard_map.insert(Point { x, y: -1 }, Hazard::Edge);
            hazard_map.insert(
                Point {
                    x,
                    y: turn.board.height,
                },
                Hazard::Edge,
            );
        }
    }

    for y in 0..turn.board.height {
        for _ in 0..turn.board.width {
            hazard_map.insert(Point { x: -1, y }, Hazard::Edge);
            hazard_map.insert(
                Point {
                    x: turn.board.width,
                    y,
                },
                Hazard::Edge,
            );
        }
    }

    // Other snakes
    for snake in &turn.board.snakes {
        for body_point in &snake.body {
            hazard_map.insert(body_point.clone(), Hazard::Body);
        }
        hazard_map.insert(snake.head.clone(), Hazard::Head);
    }

    // Me snake
    for body_point in &turn.you.body {
        hazard_map.insert(body_point.clone(), Hazard::Body);
    }

    // Foods are potential hazards
    for food_point in &turn.board.food {
        hazard_map.insert(food_point.clone(), Hazard::Food);
    }

    return hazard_map;
}

pub fn point_is_true_hazard(point: &Point, hazard_map: &HazardMap) -> bool {
    match hazard_map.get(point) {
        Some(hazard) => match hazard {
            Hazard::Food => false,
            _ => true,
        },
        None => false,
    }
}
