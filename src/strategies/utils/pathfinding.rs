use crate::strategies::utils::hazard::{point_is_true_hazard, Hazard, HazardMap};

use crate::game::models::{Movement, Point};
use std::collections::{HashMap, HashSet, VecDeque};

pub fn get_translated_point(start: &Point, movement: &Movement) -> Point {
    match movement {
        Movement::Left => Point {
            x: start.x - 1,
            y: start.y,
        },
        Movement::Right => Point {
            x: start.x + 1,
            y: start.y,
        },
        Movement::Up => Point {
            x: start.x,
            y: start.y + 1,
        },
        Movement::Down => Point {
            x: start.x,
            y: start.y - 1,
        },
    }
}

pub fn get_direction_to_point(start: &Point, end: &Point) -> Option<Movement> {
    if get_translated_point(start, &Movement::Left) == *end {
        return Some(Movement::Left);
    } else if get_translated_point(start, &Movement::Right) == *end {
        return Some(Movement::Right);
    } else if get_translated_point(start, &Movement::Up) == *end {
        return Some(Movement::Up);
    } else if get_translated_point(start, &Movement::Down) == *end {
        return Some(Movement::Down);
    }
    return None;
}

pub fn shortest_route_to_target(start: Point, hazard_map: HazardMap, target: Hazard) -> Vec<Point> {
    let mut visited = HashSet::new();
    let mut queue = VecDeque::new();
    queue.push_back(vec![start.clone()]);

    let mut route = Vec::new();
    'search: while !queue.is_empty() {
        let visiting_path = queue.pop_front().unwrap();
        let head_point = &visiting_path[visiting_path.len() - 1];
        visited.insert(head_point.clone());

        let neighbours = vec![
            get_translated_point(&head_point, &Movement::Left),
            get_translated_point(&head_point, &Movement::Right),
            get_translated_point(&head_point, &Movement::Up),
            get_translated_point(&head_point, &Movement::Down),
        ];

        for neighbour in neighbours {
            if !visited.contains(&neighbour) {
                let mut potential_path = visiting_path.clone();
                potential_path.push(neighbour.clone());

                if let Some(hazard) = hazard_map.get(&neighbour) {
                    if *hazard == target {
                        route = potential_path;
                        break 'search;
                    }
                    continue;
                }

                queue.push_back(potential_path);
            }
        }
    }

    return route;
}

struct Rectangle {
    left: Point,
    right: Point,
    up: Point,
    down: Point,
}

impl Rectangle {
    fn area(&self) -> i32 {
        let height = self.up.y - self.down.y;
        let width = self.right.x - self.left.x;
        let area = width * height;
        return area;
    }

    fn points(&self) -> Vec<Point> {
        let mut points: Vec<Point> = Vec::new();

        for x in self.left.x..=self.right.x {
            for y in self.down.y..=self.up.y {
                points.push(Point { x, y });
            }
        }

        return points;
    }
}

fn grow_rectangle(origin: Point, hazard_map: &HazardMap) -> Rectangle {
    let mut rectangle = Rectangle {
        left: origin.clone(),
        right: origin.clone(),
        up: origin.clone(),
        down: origin.clone(),
    };

    'grow_up: loop {
        let potential_point = get_translated_point(&rectangle.up, &Movement::Up);
        if point_is_true_hazard(&potential_point, hazard_map) {
            break 'grow_up;
        }
        rectangle.up = potential_point;
    }

    'grow_down: loop {
        let potential_point = get_translated_point(&rectangle.down, &Movement::Down);
        if point_is_true_hazard(&potential_point, hazard_map) {
            break 'grow_down;
        }
        rectangle.down = potential_point;
    }

    'grow_left: loop {
        let potential_left_point = get_translated_point(&rectangle.left, &Movement::Left);
        for y in rectangle.down.y..=rectangle.up.y {
            let testing_point = Point {
                x: potential_left_point.x,
                y,
            };

            if point_is_true_hazard(&testing_point, hazard_map) {
                break 'grow_left;
            }
        }
        rectangle.left = potential_left_point;
    }

    'grow_right: loop {
        let potential_right_point = get_translated_point(&rectangle.right, &Movement::Right);
        for y in rectangle.down.y..=rectangle.up.y {
            let testing_point = Point {
                x: potential_right_point.x,
                y,
            };

            if point_is_true_hazard(&testing_point, hazard_map) {
                break 'grow_right;
            }
        }
        rectangle.right = potential_right_point;
    }

    return rectangle;
}

pub fn find_largest_rectangles(
    hazard_map: HazardMap,
    board_width: i32,
    board_height: i32,
) -> HashMap<Point, i32> {
    let mut rectangles_by_point: HashMap<Point, i32> = HashMap::new();

    for x in 0..board_width {
        for y in 0..board_height {
            let origin_tile = Point { x, y };

            if !rectangles_by_point.contains_key(&origin_tile)
                && !point_is_true_hazard(&origin_tile, &hazard_map)
            {
                let rectangle = grow_rectangle(origin_tile, &hazard_map);
                let rectangle_area = rectangle.area();

                for tile in rectangle.points() {
                    match rectangles_by_point.get(&tile) {
                        Some(existing_area) => {
                            if rectangle_area > *existing_area {
                                rectangles_by_point.insert(tile, rectangle_area);
                            }
                        }
                        None => {
                            rectangles_by_point.insert(tile, rectangle_area);
                        }
                    }
                }
            }
        }
    }

    return rectangles_by_point;
}
