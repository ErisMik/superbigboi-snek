pub mod hazard;
pub mod pathfinding;
pub mod wmm;

use hazard::*;
use pathfinding::*;

use crate::game::models::{Movement, Turn};

pub fn is_valid_movement(turn: &Turn, movement: &Movement) -> bool {
    let hazard_map = generate_hazard_map(turn);
    let my_head_point = turn.you.head.clone();
    let potential_point = get_translated_point(&my_head_point, movement);

    if let Some(hazard) = hazard_map.get(&potential_point) {
        match hazard {
            Hazard::Food => return true,
            _ => return false,
        }
    } else {
        return true;
    }
}

pub fn is_empty_movement(turn: &Turn, movement: &Movement) -> bool {
    let hazard_map = generate_hazard_map(turn);
    let my_head_point = turn.you.head.clone();
    let potential_point = get_translated_point(&my_head_point, movement);

    if let Some(_) = hazard_map.get(&potential_point) {
        return false;
    } else {
        return true;
    }
}

pub fn step_to_closest_food(turn: &Turn) -> Option<Movement> {
    let hazard_map = generate_hazard_map(turn);
    let my_head_point = turn.you.head.clone();

    let route = shortest_route_to_target(my_head_point, hazard_map, Hazard::Food);

    if route.len() >= 2 {
        return get_direction_to_point(&route[0], &route[1]);
    } else {
        return None;
    }
}

pub fn step_to_largest_empty_rectangle(turn: &Turn) -> Option<Movement> {
    let hazard_map = generate_hazard_map(turn);
    let my_head_point = turn.you.head.clone();
    let board_width = turn.board.width;
    let board_height = turn.board.height;

    let rectangle_sizes = find_largest_rectangles(hazard_map, board_width, board_height);

    let left_rectangle_size =
        match rectangle_sizes.get(&get_translated_point(&my_head_point, &Movement::Left)) {
            Some(size) => *size,
            None => 0,
        };

    let right_rectangle_size =
        match rectangle_sizes.get(&get_translated_point(&my_head_point, &Movement::Right)) {
            Some(size) => *size,
            None => 0,
        };

    let up_rectangle_size =
        match rectangle_sizes.get(&get_translated_point(&my_head_point, &Movement::Up)) {
            Some(size) => *size,
            None => 0,
        };

    let down_rectangle_size =
        match rectangle_sizes.get(&get_translated_point(&my_head_point, &Movement::Down)) {
            Some(size) => *size,
            None => 0,
        };

    // Find best direction
    let mut largest_rectangle_direction = Movement::Left;
    let mut largest_rectangle_size = left_rectangle_size;

    if right_rectangle_size > largest_rectangle_size {
        largest_rectangle_direction = Movement::Right;
        largest_rectangle_size = right_rectangle_size;
    }

    if up_rectangle_size > largest_rectangle_size {
        largest_rectangle_direction = Movement::Up;
        largest_rectangle_size = up_rectangle_size;
    }

    if down_rectangle_size > largest_rectangle_size {
        largest_rectangle_direction = Movement::Down;
    }

    return Some(largest_rectangle_direction);
}
