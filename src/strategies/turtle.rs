use crate::game::models::{Movement, Turn};
use crate::strategies::utils;
use crate::strategies::Strategy;
use log::*;

pub struct TurtleStrategy {}

impl TurtleStrategy {
    pub fn new() -> Self {
        return TurtleStrategy {};
    }
}

const HAZARD_WEIGHT: i32 = -10;
const FOOD_WEIGHT: i32 = 5;
const SPACIEST_POTENTIAL_WEIGHT: i32 = 3;
const CIRCLE_WEIGHT: i32 = 3;
const LAST_MOVEMENT_WEIGHT: i32 = 1;

impl Strategy for TurtleStrategy {
    fn next_move(&self, turn_history: &Vec<Turn>, movements: &Vec<Movement>) -> Movement {
        let latest_turn = &turn_history[turn_history.len() - 1];
        let last_movement = &movements[movements.len() - 1];

        info!("Turn number: {}", latest_turn.turn);
        info!("Did {:?} last time", last_movement);

        let mut wmm = utils::wmm::WeightedMovementMatrix::new();

        // If no better option, go straight
        wmm.shift_score(last_movement, LAST_MOVEMENT_WEIGHT);

        // Circle by default
        let circle_movement = match last_movement {
            Movement::Left => Movement::Up,
            Movement::Right => Movement::Down,
            Movement::Up => Movement::Right,
            Movement::Down => Movement::Left,
        };
        wmm.shift_score(&circle_movement, CIRCLE_WEIGHT);
        info!("Going {:?} to circle", circle_movement);

        // Get food if hungry
        if latest_turn.you.health < 25 {
            info!("HUNGRY!");

            if let Some(food_movement) = utils::step_to_closest_food(&latest_turn) {
                wmm.shift_score(&food_movement, FOOD_WEIGHT);
                info!("Moving {:?} towards food", food_movement);
            }
        }

        // Avoid those obstacles
        if !utils::is_valid_movement(&latest_turn, &Movement::Left) {
            wmm.shift_score(&Movement::Left, HAZARD_WEIGHT);
        }
        if !utils::is_valid_movement(&latest_turn, &Movement::Right) {
            wmm.shift_score(&Movement::Right, HAZARD_WEIGHT);
        }
        if !utils::is_valid_movement(&latest_turn, &Movement::Up) {
            wmm.shift_score(&Movement::Up, HAZARD_WEIGHT);
        }
        if !utils::is_valid_movement(&latest_turn, &Movement::Down) {
            wmm.shift_score(&Movement::Down, HAZARD_WEIGHT);
        }

        // Break ties by heading down the largest empty space
        if wmm.only_two_positive_options() {
            if let Some(spaciest_movement) = utils::step_to_largest_empty_rectangle(&latest_turn) {
                info!("Longest potential {:?}", spaciest_movement);
                wmm.shift_score(&spaciest_movement, SPACIEST_POTENTIAL_WEIGHT);
            }
        }

        // Make the final decision
        let movement = wmm.select_best_movement().unwrap();
        info!("Final choice: {:?} {:?}", movement, wmm);
        return movement;
    }
}
