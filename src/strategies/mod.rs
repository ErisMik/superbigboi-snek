pub mod turtle;
pub mod utils;

use crate::game::models::{Movement, Turn};

pub trait Strategy: Send {
    fn next_move(&self, turn_history: &Vec<Turn>, movements: &Vec<Movement>) -> Movement;
}
