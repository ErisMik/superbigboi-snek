use serde::{Deserialize, Serialize};

#[derive(Hash, Clone, Deserialize, PartialEq, Eq, Debug)]
pub struct Point {
    pub x: i32,
    pub y: i32,
}

#[derive(Clone, Deserialize, PartialEq, Eq, Debug)]
pub struct Turn {
    pub game: Game,
    pub turn: u32,
    pub board: Board,
    pub you: Snake,
}

#[derive(Hash, Clone, Deserialize, PartialEq, Eq, Debug)]
pub struct Game {
    pub id: String,
    pub timeout: i32,
}

#[derive(Clone, Deserialize, PartialEq, Eq, Debug)]
pub struct Board {
    pub height: i32,
    pub width: i32,
    pub food: Vec<Point>,
    pub hazards: Vec<Point>,
    pub snakes: Vec<Snake>,
}

#[derive(Clone, Deserialize, PartialEq, Eq, Debug)]
pub struct Snake {
    pub id: String,
    pub name: String,
    pub health: i32,
    pub body: Vec<Point>,
    pub latency: String,
    pub head: Point,
    pub length: i32,
    pub shout: String,
}

#[derive(Hash, Clone, Serialize, Deserialize, PartialEq, Eq, Debug)]
#[serde(rename_all(serialize = "lowercase", deserialize = "lowercase"))]
pub enum Movement {
    Left,
    Right,
    Up,
    Down,
}
