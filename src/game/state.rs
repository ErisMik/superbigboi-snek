use std::collections::HashMap;

use crate::game::models::{Game, Movement, Turn};
use crate::strategies::Strategy;

pub struct GameState {
    turns: Vec<Turn>,
    strategy: Box<dyn Strategy>,
    movements: Vec<Movement>,
}

impl GameState {
    pub fn new(strategy: Box<dyn Strategy>) -> Self {
        GameState {
            strategy,
            turns: Vec::new(),
            movements: vec![Movement::Up],
        }
    }

    pub fn add_turn(&mut self, turn: Turn) {
        self.turns.push(turn);
    }

    pub fn add_move(&mut self, movement: Movement) {
        self.movements.push(movement);
    }

    pub fn next_move(&self) -> Movement {
        return self.strategy.next_move(&self.turns, &self.movements);
    }
}

pub struct GameStateManager {
    game_states: HashMap<Game, GameState>,
}

impl GameStateManager {
    pub fn new() -> Self {
        GameStateManager {
            game_states: HashMap::<Game, GameState>::new(),
        }
    }

    pub fn insert_gamestate(&mut self, game: Game, game_state: GameState) {
        self.game_states.insert(game, game_state);
    }

    pub fn get_gamestate(&mut self, game: &Game) -> &mut GameState {
        return self.game_states.get_mut(game).unwrap();
    }
}
