#![feature(proc_macro_hygiene, decl_macro)]

// External crates
#[macro_use]
extern crate rocket;
extern crate rocket_contrib;

// Modules
mod game;
mod requests;
mod responses;
mod strategies;

// Uses
use game::state::{GameState, GameStateManager};
use rocket::State;
use rocket_contrib::json::Json;
use std::sync::{Arc, Mutex};

#[get("/")]
fn index() -> Json<responses::Index> {
    Json(responses::Index::new(
        "1".to_string(),
        "erismik".to_string(),
        "#EC9AF6".to_string(),
        responses::HeadType::Tongue,
        responses::TailType::RoundBum,
    ))
}

#[post("/start", format = "json", data = "<req>")]
fn start(
    req: Json<requests::Turn>,
    game_state_manager: State<Arc<Mutex<GameStateManager>>>,
) -> &'static str {
    let turn = req.into_inner();

    let game = turn.game.clone();
    let strategy = Box::new(strategies::turtle::TurtleStrategy::new());
    let mut gamestate = GameState::new(strategy);
    gamestate.add_turn(turn);

    let mut game_state_manager = game_state_manager.lock().unwrap();
    game_state_manager.insert_gamestate(game, gamestate);

    return "Let's get going!";
}

#[post("/move", format = "json", data = "<req>")]
fn movement(
    req: Json<requests::Turn>,
    game_state_manager: State<Arc<Mutex<GameStateManager>>>,
) -> Json<responses::Move> {
    let turn = req.into_inner();

    let mut game_state_manager = game_state_manager.lock().unwrap();
    let game_state = game_state_manager.get_gamestate(&turn.game);

    game_state.add_turn(turn);
    let movement = game_state.next_move();

    game_state.add_move(movement.clone());
    return Json(responses::Move::new(movement));
}

#[post("/end")]
fn end() -> &'static str {
    return "GG EZ";
}

#[post("/ping")]
fn ping() -> &'static str {
    return "Yes sir!";
}

fn rocket() -> rocket::Rocket {
    rocket::ignite()
        .manage(Arc::new(Mutex::new(GameStateManager::new())))
        .mount("/", routes![index, start, movement, end, ping])
}

fn main() {
    rocket().launch();
}
