# SuperBigBoi Snek

My learning multi-strategy snake for [BattleSnake](https://play.battlesnake.com/) competitions. See me [here!](https://play.battlesnake.com/u/erismik/big-boi-snek/)

## Usage
### Cargo
The snake can be built and run using `cargo` by running:
```
$ cargo build
$ cargo run
```

### Docker (Preferred)
`SuperBigBoi Snek` is designed to be deployed using docker, use the included Makefile to build the docker image `itseris/battlesnek:latest`:
```
$ make build
$ make run
```

## API
`SuperBigBoi Snek` implements the battlesnake V1 API. You can view the documentation for that API [here](https://docs.battlesnake.com/references/api).

## Strategies
Snakes can employ a series of different strategies.
By default, this snake will pick a random strategy.
If this snake has a history of beating the other snake with a specific strategy, it will use that one instead.

Strategy notes don't include global behaviors such as "avoid walls" and "avoid other snakes tails".

### Turtle Strategy
The idea behind this strategy is to out-survive the other snakes and hope they make a mistake first.
- Spin in a circle
- If hungry, grab the closest food
- If snake comes close, move out of the way
- Always look to be in the most empty space

