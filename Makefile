build:
	docker build . -t itseris/battlesnek:latest

push: build
	docker push itseris/battlesnek:latest

run: build
	docker run -it --init -p 8080:8080 itseris/battlesnek:latest