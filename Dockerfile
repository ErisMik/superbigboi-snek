FROM rust:latest AS builder

RUN mkdir -p /battlesnek
WORKDIR /battlesnek

RUN rustup update nightly
RUN rustup default nightly

COPY Cargo.toml Cargo.lock ./
COPY Rocket.toml ./ 
COPY src/ ./src/

RUN cargo build --release


FROM debian:buster AS runner

RUN mkdir -p /battlesnek
WORKDIR /battlesnek

# RUN apt-get update -y && apt-get install -y openssl

COPY --from=builder /battlesnek/Rocket.toml /battlesnek
COPY --from=builder /battlesnek/target/release/superbigboi-snek /battlesnek

ENTRYPOINT ["/battlesnek/superbigboi-snek"]
